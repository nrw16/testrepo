# This is the root folder of a standard SDLE git repository structure.

This repository is either a project repository, or a code repository.

Project repositories are meant to contain work related to funded studies being performed by members of the SDLE research center.

Code repositories are meant to contain code under development that is being utilized by several projects.

Please browse within each folder to learn more about the intended purpose of each folder in the standard structure.

This folder structure has been designed to accomodate each type of file you may need to create and modify - please do not create additional folders in the structure, and please pay attention to naming conventions when creating new files.

## NOTE:  How to change the README content:

This content, and that of other README files contained in this repositiory, can be changed by first modifying the file 'readme-xx.md' in the 'inst/extdata/' folder in the package file structure (where 'xx' is the name of the folder containing the readme, with the exception of this readme which is named 'readme-root.md'), and then running the 'writemdrdas()' function with the path of the repository directory (where the DESCRIPTION file is) as the argument.

Running this function will update the 'xx_mdlv.rda' file of the 'repomatic' package, which contains the lines of text in this markdown as a vector of character elements ('xx_mdlv'), to match the contents of the 'inst/extdata/readme-xx.md' file.

This work is legally bound by the following software license: [CC-A-NS-SA-4.0][1] [^1]  
Please see the LICENSE.txt file, in the root of this repository, for further details.

[1]: https://creativecommons.org/licenses/by-nc-sa/4.0/ "CC-A-NS-SA-4.0"


[^1]: [CC-A-NS-SA-4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
